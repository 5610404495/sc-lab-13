package P1;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Management {
	
	private Lock mLock;
	private Condition mCondition;
	private LinkedList<Integer> Link;

	
	public Management() {
		mLock = new ReentrantLock();
		mCondition = mLock.newCondition();
		Link = new LinkedList<Integer>();
	}
	
	
	public void add() {
		mLock.lock();
		try {
			Link.add(1);
			System.out.println("Element add in LinkedList");
			mCondition.signalAll();
		}
		finally {
			mLock.unlock();
		}
	}
	
	public void Del() throws InterruptedException {
		mLock.lock();
		try {
			while(Link.size()<1){
				mCondition.await();

			}
			Link.removeLast();
			System.out.println("Element remove in LinkedList");
				
		}
		finally {
			mLock.unlock();
		}
	}

	
}
