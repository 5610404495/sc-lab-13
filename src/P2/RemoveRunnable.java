package P2;

public class RemoveRunnable implements Runnable{
	private Contact contact;
	private int count;
	private String n;
	private static int DELAY = 1;
	
	public RemoveRunnable(Contact c,int count,String n){
		this.contact = c;
		this.count = count;
		this.n = n;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			for(int i=1;i<=count;i++){
				contact.remove(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error Interrupted");
		}
	}
	
	
}
