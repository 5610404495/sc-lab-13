package P2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Contact {
	private Lock BalanceLock;
	private Condition enoughCondition;
	private Queue qu;

	public Contact() {
		BalanceLock = new ReentrantLock();
		enoughCondition = BalanceLock.newCondition();
		qu = new Queue(10);
	}

	public void add(String a) throws InterruptedException {
		try {
			BalanceLock.lock();

			while (qu.getSize() > 10) {
				enoughCondition.await();
			}
			qu.add(a);

			System.out.print("Add " + a + "\n");
			System.out.println(qu.toString());
			enoughCondition.signalAll();
		} 
		finally {
			BalanceLock.unlock();
		}
	}

	public void remove(String a) throws InterruptedException {
		try {
			BalanceLock.lock();
			while (qu.getSize() <= 0) {
				enoughCondition.await();
			}
			qu.remove(0);
			System.out.print("remove " + a+"\n");
			System.out.println(qu.toString());
			enoughCondition.signalAll();
		} 
		finally {
			BalanceLock.unlock();
		}
	}
}
