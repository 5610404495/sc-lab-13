package P2;

import java.util.ArrayList;

public class Queue {
	private ArrayList<String> list;
	private int size;
	
	public Queue(int s){
		list = new ArrayList<String>();		
		this.size = s;
	}
	public int getSize(){
		return list.size();
	}	
	public void add(String a){
		list.add(a);
	}	
	public void remove(int a){
		list.remove(a);		
	}		
	public String toString(){
		return list.toString();
	}
}
